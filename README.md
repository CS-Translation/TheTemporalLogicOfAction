<script type="text/javascript" src="http://cdn.mathjax.org/mathjax/latest/MathJax.js?config=default"></script>



# 参考资源

[TLA+ Toolbox User's Guide](https://tla.msr-inria.inria.fr/tlatoolbox/doc/contents.html)

[CSDN Markdown](http://write.blog.csdn.net/mdeditor?ticket=ST-75344-TAk4CjG63qgmEBTmcA6f-passport.csdn.net)

[LaTex语法](https://math.meta.stackexchange.com/questions/5020/mathjax-basic-tutorial-and-quick-reference)

[如何在Ubuntu系统下安装使用LaTeX](https://jingyan.baidu.com/article/7c6fb4280b024180642c90e4.html)

[Ubuntu下Latex安装及中文配置](http://www.jianshu.com/p/d185aad1f915)

[Ubuntu下LaTex中文环境配置](http://blog.sina.com.cn/s/blog_7abb28aa0101hta7.html)


