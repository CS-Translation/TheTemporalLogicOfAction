# The Temporal Logic of Actions
# 行为时序逻辑

LESLIE LAMPORT
Digital Equipment Corporation

标签（空格分隔）： TLA+

---
The temporal logic of actions (TLA) is a logic for specifying and reasoning about concurrent
systems. Systems and their properties are represented in the same logic, so the assertion that
a system meets its specification and the assertion that one system implements another are both
expressed by logical implication. TLA is very simple; its syntax and complete formal semantics are
summarized in about a page. Yet, TLA is not just a logician’s toy; it is extremely powerful, both
in principle and in practice. This report introduces TLA and describes how it is used to specify
and verify concurrent algorithms. The use of TLA to specify and reason about open systems will
be described elsewhere.
Categories and Subject Descriptors: D.2.4 [**Software Engineering**]: Program Verification—
*correctness proofs*; F.3.1 [**Logics and Meanings of Programs**]: Specifying and Verifying and
Reasoning about Programs—*Specification techniques*
General terms: Theory, Verification
Additional Key Words and Phrases: Concurrent programming, liveness properties, safety properties

## 1. LOGIC VERSUS PROGRAMMING
A concurrent algorithm is usually specified with a program. Correctness of the algorithmmeans that the program satisfies a desired property. We propose a simpler approach in which both the algorithm and the property are specified by formulas in a single logic. Correctness of the algorithm means that the formula specifying the algorithm implies the formula specifying the property, where implies is ordinary logical implication.
    We are motivated not by an abstract ideal of elegance, but by the practical problem of reasoning about real algorithms. Rigorous reasoning is the only way to avoid subtle errors in concurrent algorithms, and we want to make reasoning as simple as possible by making the underlying formalism simple.
    How can we abandon conventional programming languages in favor of logic if the algorithm must be coded as a programto be executed?The answer is that we almost always reason about an abstract algorithm, not about a concurrent program that is actually executed. A typical example is the distributed spanning-tree algorithm used in the Autonet local area network [Schroeder et al. 1990]. The algorithm can be described in about one page of pseudo-code, but its implementation required about 5000 lines of C code and 500 lines of assembly code. [^footnote] Reasoning about 5000 lines of C would be a herculean task, but we can reason about a one-page abstract algorithm. By starting from a correct algorithm, we can avoid the timingdependent synchronization errors that are the bane of concurrent programming. If the algorithms we reason about are not real, compilable programs, then they do not have to be written in a programming language.

But, why replace a programming language by logic? Aren’t programs simpler than logical formulas? T he answer is no. Logic is the formalization of everyday mathematics, and everyday mathematics is simpler than programs. Consider the Pascal statement $ y := x + 1 $.  Using the convention that $ y' $ denotes the new value of $ y $, we can rewrite this statement as the mathematical formula $ y' = x+1 $. Many readers will think that the Pascal statement and the formula are equally simple. They are wrong. The formula is much simpler than the Pascal statement. Equality is a simple concept that five-year-old children understand. Assignment $(:=)$ is a complicated concept that university students find  difficult. Equality obeys simple algebraic laws; assignment doesn’t. If we assume that all variables are  integervalued, we can subtract y from both sides of the formula to obtain the equivalent formula $ 0 = x + 1 − y'$. Trying this with the Pascal statement yields the absurdity $ 0 := x + 1 − y $.

A programming language may use mathematical terms like function, but the constructs they represent are not as simple as the corresponding mathematical concepts. Mathematical functions are simple; children in the United States learn about them at the age of twelve. Pascal functions are complicated, involving concepts like call by reference, call by value, and aliasing; it is unlikely that many university
students understand them well. Advocates of so-called functional programming languages often claim that they just use ordinary mathematical functions, but try explaining to a twelve-year-old how evaluating a mathematical function can display a character on her computer screen.
    Since real languages like Pascal are so complicated, methods for reasoning about algorithms are usually based on toy languages. Although simpler than real programming 
languages, toy languages are more complicated than simple logic. Moreover, their resemblance to real languages can be dangerously misleading. In toy languages, the Hoare triple $ \{x = 0\} y := x + 1 \{y = x + 1\} $ is valid, which means that executing y := x + 1 in a state in which x equals 0 produces a state in which y equals x + 1. However, in Pascal, the program fragment
$ x := 0; y := x + 1 $; write $ (y, x + 1) $ can print two different values when used in certain contexts, even if x and y are variables of type integer. The programmer who tries using toy-language rules to reason about real Pascal programs is in for a rude surprise.
    We do not mean to belittle programming languages. They are complicated because they have a difficult job to do. Mathematics can be based on simple concepts like functions. Programming languages cannot, because they must allow reasonably simple compilers to translate programs into reasonably efficient code for complex computers. Real languages must embrace difficult concepts like the distinction between values and locations, which leads to call-by-reference arguments andaliasing—complications that have no counterpart in simple mathematics.  Programming languages are necessary for writing real programs; but mathematics offers a
simpler alternative for reasoning about concurrent algorithms.
    To offer a practical alternative to programming languages, a logic must be both simple and expressive. There is no point trading a programming language for a logic that is just as complicated and hard to understand. Furthermore, a logic that achieves simplicity at the expense of needed expressiveness will be impractical because the formulas describing real algorithms will be too long and complicated to understand.

The logic that we propose for reasoning about concurrent algorithms is the temporal logic of actions, abbreviated as **TLA**. All TLA formulas can be expressed in terms of familiar mathematical operators (such as ∧) plus three new ones: $'$ (prime), $ \Box $, $\exists$. TLA is simple enough that its syntax and complete formal semantics can be written in about a page. Almost all that is needed to specify and reason about algorithms in TLA—its syntax, formal semantics, derived notation, and axioms and proof rules—appears in Figures 4 and 5 of Section 5.6 and Figure 9 of Section 8.2. (Missing from those figures are the rules for adding auxiliary variables, mentioned in Section 8.3.2.)
    Logic is a tool. Its true test comes with use. Although TLA and its proof rules can be described formally in a couple of pages, such a description would tell you nothing about how TLA is used. In this article, we develop TLA as a method of describing and reasoning about concurrent algorithms. We limit ourselves to simple examples, so we can only hint at how TLA works with real algorithms. TLA combines two logics: a logic of actions, described in Section 2, and a standard temporal logic, described in Section 3. TLA is easiest to explain in terms of a logic called RTLA, which is defined in Section 4. We describe TLA itself and illustrate its use in Sections 5–8. Section 9 mentions further applications and discusses what TLA can and cannot do, and Section 10 relates TLA to other formalisms.

## 2. THE LOGIC OF ACTIONS
### 2.1 Values, Variables, and States
Algorithms manipulate data. We assume a collection Val of values, where a value is a data item. The collection Val includes numbers such as 1, 7, and −14, strings like “abc”, and sets like the set Nat of natural numbers. We don’t bother to define Val precisely, but simply assume that it contains all the values needed for our examples (Note [^footnote] 1). We also assume the booleans true and false, which for technical reasons are not considered to be values.
    We think of algorithms as assigning values to variables. We assume an infinite set Var of variable names. We won’t describe a precise syntax for generating variable names, but will simply use names like $x$ and $sem$.
    A logic consists of a set of rules for manipulating formulas. To understand what the formulas and their manipulation mean, we need a semantics. A semantics is given by assigning a semantic meaning $ \llbracket $ F $ \rrbracket $  to each syntactic object F in the logic.
    The semantics of our logic is defined in terms of states. A state is an assignment of values to variables—that is, a mapping from the set Var of variable names to the collection Val of values. Thus a state $s$ assigns a value $s(x)$ to a variable $x$. The collection of all possible states is denoted $St$.
    We write s$\llbracket$ $x$ $\rrlbracket$ to denote $s(x)$. Thus, we consider the meaning $\llbracket$ $x$ $\rrlbracket$ of the variable $x$ to be a mapping from states to values, using a postfix notation for  function application. States and values are purely semantic concepts; they do not appear explicitly in formulas.

### 2.2 State Functions and Predicates
A state function is a nonboolean expression built from variables and constant symbols—for example, $x^2 + y − 3$ (Note 2). The meaning [[f]] of a state function $f$ is a mapping from the collection St of states to the collection Val of values. For example, [[$x^2 + y − 3$]] is the mapping that assigns to a state s the value $(s[[x]])^2$ + $s[[y]]$ − 3, where 2 and 3 are constant symbols, and 2 and 3 are the values that they represent. We will not bother distinguishing between constant symbols and their values. We use a postfix functional notation, letting $s[[f]]$ denote the value that $[[f]]$ assigns to state $s$. The semantic definition is 
$$
s[[f]] \triangleq f(\forall v : s[[v]] / v)
$$
where $f(\forall v : s[[v]] / v)$ denotes the value obtained from $f$ by substituting $s[[v]]$ for $v$, for all variables $v$. (The symbol $\triangleq$ means equals by definition.)
A variable $x$ is a state function — the state function that assigns the value $s[[x]]$ to the state $s$. The definition of $[[f]]$ for a state function f therefore extends the definition of $[[x]]$ for a variable $x$.
A state predicate, called a predicate for short, is a boolean expression built from variables and constant symbols—for example, $x^2 = y − 3$ and $x \in Nat$. The meaning $[[P]]$ of a predicate $P$ is a mapping from states to booleans, so $s[[P]]$ equals true or false for every state $s$. We say that a state s satisfies a predicate $P$ iff (if and only if) $s[[P]]$ equals true. State functions correspond both to expressions in ordinary programming languages and to subexpressions of the assertions used in ordinary program verification. Predicates correspond both to boolean-valued expressions in programming languages and to assertions.

## 2.3 Actions





[^footnote]: 1Assembly code was needed because C has no primitives for sending messages across wires.
[^footnote]: Notes appear at the end of the article
